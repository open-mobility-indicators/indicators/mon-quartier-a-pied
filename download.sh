##!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <parameter_profile_file> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
PARAMETER_PROFILE_FILE=$1
TARGET_DIR=$2

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi


if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi


echo "download data to target dir: $TARGET_DIR"

cd $TARGET_DIR

# Download pop density GeoJSON file
TERRITORY=`jq -r '.territory' $PARAMETER_PROFILE_FILE`
echo "TERRITORY: $TERRITORY"

REQ="https://api.openmobilityindicators.org/indicator-data?parameter_profile="$TERRITORY"&slug=population-density-from-cycles&sort=created_at%3Adesc&page=1&size=1"

wget -q "$REQ" -O $TARGET_DIR/response
RAWID=`jq -r '.indicator_data| .items| .[0]| .id' $TARGET_DIR/response`
echo "RAWID: $RAWID"

POPDENSITY_URL="https://files.openmobilityindicators.org/indicator-data/"$RAWID"/"pop_density.geojson
echo "POPDENSITY_URL: $POPDENSITY_URL"

echo "download pop density geojson file to target dir: $TARGET_DIR"
wget -q "$POPDENSITY_URL" -O pop_density.geojson
echo "done."
